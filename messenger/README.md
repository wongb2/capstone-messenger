# README.md - CPS490 Report 

University of Dayton

Department of Computer Science

CPS 490 - Capstone I, Semester Year

Instructor: Dr. Phu Phung


## Capstone I Project 


# The Messenger Application


# Team members

* Brandon Wong <wongb2@udayton.edu>
* David Puzder <puzderd1@udayton.edu>
* Sydney Jenkins <jenkinss4@udayton.edu>
* Devin Porter <porterd3@udayton.edu>


# Project Management Information

Management board (private access): <https://trello.com/b/aqMlNbvH/team-1-sprint-board>

Source code repository (private access): <https://bitbucket.org/cps490f20-team1/cps490-project-team1/src/master/>


## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
|09/10/2020|  0.0          | Sprint 0     |
|10/02/2020| 0.1		   | Sprint 1	  |


# Overview

![Architecture Figure](https://bitbucket.org/cps490f20-team1/cps490-project-team1/raw/383e729bb86d9bd906c83b35a2292784962c7577/images/Screen%20Shot%202020-09-08%20at%207.31.45%20PM.png)

Technology?
– Node.js

Type of software?
– Web Application with WebSocket
– Use a web browser
– Accessed through a URL
– Reside on a Web server
– Use standard IP protocols

How many layers?
– Client/Server 
– Traditional two-tier architecture
– Software design with part of the application on a server
and part on the client

# System Analysis

Sprint 1
Our system is made up of the backend web server (ChatServer.JS) and the front end client interface (Index.html. We have multiple ChatUI pages that are used for public and private chat.
The saved usernames and passwords are saved in a Dictionary in the ChatServer.js file that allows for users to be validated as registered.
We use Socket.io to allow real-time bidirectional communication between the browser and the server, using Websocket.

## User Requirements

* Send Direct Message
	* Users can type, text, and send to a single receiver
	* Description: User inputs name of another user in the system. The system sends message input and logs both usernames, time sent, time recieved, 
	* and message content. 
* Public Chat
	* Users can type, text, and send to a public chatroom with a character limit
	* Description: User enters the public chatroom and user sends message and all members of chatroom recieve it.
* Registration
	* Users can register to the system
	* Description: Users enter new account data and the system assigns a new account number, a new user record, and new account record.
* Login
	* Users can login with registered account and close window when finished
	* Description: User enters already registered account data and the system retrieves account and record information.
* Real-Time Messaging
	* Users should receive the messages sent to them in real time with a timestamp
	* Description: Users enter another user's name and can input message to send to another user. Can also retrieve messages sent from other users.
	* System creates record of times sent and recieved and allows messages back and forth almost instantly.
* Message History
	* Users can view the message history
	* Description: Users can select message history to view records of previously sent messages between users in the system.
* Read Receipts
	* Users can see if the sent messages have been read
	* Description: Users can select messages to see if messages were viewed by whom they were sent to and if viewed, they can see the time the message was viewed.

## Use cases

User Stories:

* Register for an account
	* As an unregistered user, I am able to register for a new account within the system.
* Login
	* As a registered user, I can log into the system using a username and password.
* Send Direct Message
	* As a registered user, I can send a message directly to another registered user, securely and without any other user viewing that message. 
* View message history
	* As a registered user, I can review previous messages that I have sent, and have been sent to me, whether in group or direct messaging.
* Send Public Message
	* As a registered user, I can send a message to all logged in users at once, and any messages sent back to me will also go to all of those people.
* Real-Time Messaging
	* As a registered user, I can receive messages as they are sent to me.
* Read Receipts
	* As a user who has sent a message, I can see who has and has not read the message.






Overview Use Case Diagram
![Use Case Diagram](https://trello-attachments.s3.amazonaws.com/5f514012901b49388c7d232d/894x992/3f8e26a350204116060680940ee1dcb8/Screen_Shot_2020-09-25_at_5.04.13_PM.png)
	
Use Case Discriptions
![Registration Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b01268db90b1eb8e22e/fcacedf9f71a9a9e8d39f9a094859312/Screen_Shot_2020-09-25_at_5.12.11_PM.png)
![Login Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b209807b273a0e3c158/71444c6d535d3e34da77ca57a9217e79/Screen_Shot_2020-09-25_at_5.12.48_PM.png)
![Receive a message Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b3fb02560171c51d795/c7388a65cdbecf937305a6a7863f45dc/Screen_Shot_2020-09-25_at_5.15.39_PM.png)
![Send a Direct Message Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511a6895e1fb713de81f0f/22dab9edd1cfb99dc2874707d64bb0d4/Screen_Shot_2020-09-25_at_5.09.47_PM.png)
![Send to the Receiver Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511a6895e1fb713de81f0f/9faac0085415978c0d80e0a01c3e89f8/Screen_Shot_2020-09-25_at_5.10.00_PM.png)
![View Message History Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b83e88aef2e54f93a8e/b88330bc7e0f7d8391929b2786bac735/Screen_Shot_2020-09-25_at_5.13.42_PM.png)
![View Read Receipts Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b9f550c903090e633fb/7b8cfd0a585fb13299787c7895b6652b/Screen_Shot_2020-09-25_at_5.14.14_PM.png)
![Send Public Message Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511ae002541e29b70dcdff/aabaf909d2bc57c45f1e995ed703be79/Screen_Shot_2020-09-25_at_5.11.33_PM.png)


# System Design

SPRINT 1
Currently, our system is designed to be a messenger app for registered users to chat publicly and privately as they please. Users log in with a username and password and once they are verified, the chat box appears and they can send and recieve messages. If they would like to chat privately, they select the button that says that and then enter the name of the recipient whom they wish to message. When a message is sent, there is a timestamp. When a user joins the chat, their name appears up top. When they are done chatting, they can select to "close window" which closes out the tab. We will continue to update as we progress through the sprints.

### Login Sequence Diagram
![Login Sequence Diagram](https://trello-attachments.s3.amazonaws.com/5f58131b90edaf09eab4ac6b/5f6e60b732e8a517fe529fc9/5e0783d32d77c7046c05b897cd6fd77b/Login_Sequence_Diagram.png)

### Public Chat Sequence Diagram
![Public Chat Sequence Diagram](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511ae002541e29b70dcdff/3e8e57f0480e39da6a1f98d2967fcf1c/Public_Chat_Sequence_Diagram.png)

## Use-Case Realization

We have implemented all the processes identified in the Use Cases in our webpage. This is evident in the implementation and the page itself.

## Database 

_(Start from Sprint 3, keep updating)_

## User Interface

SPRINT 1 
Right now, our webpage is pink with a username and password block for users to login. Once they are validated as registered users, a seperate chat box appears that allows them to send messages. This chatbox is hidden from them until they login correctly. This is to keep the page clean. When a user logs in, their username appears in a welcome message. The user list is seperate from the chat box to keep the page clean and easy to read. Users can select to go to private chat from a seperate button. When they do this, it takes them to a different ChatUI, as indicated in the instructions. 

# Implementation

_(Start from Sprint 1, keep updating)_

For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments. 

SPRINT 1
For this spring, we are implementing the use of NodeJs to create our chat messenging system. The ChatServer.Js file is the server program that allows for dynamic content. This is what allows us to message across various pages. The Index.html is the client interface that displays the webpage to the user. We use JavaScript in this file to run in the web browser.

Login Server Side:
var DataLayer = {
    info: 'Data Layer Implementation for Messenger',
    checklogin(username, password){
        var userDict={"Devin":"Porter", "David":"Puzder", "Sydney":"Jenkins", "Brandon":"Wong", "Phu":"Phung", "Anna":"Duricy"};
        console.log("checklogin: "+username+ "/" + password);
        if (password === userDict[username]){
            return true;
        }
		
This is what allows users to login from the server side (ChatServer.js). The username and password combination are saved in the dictionary. When the password matches the username, the DataLayer returns true and the user is validated. 

Login Client Side:
   function login(){
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
            document.getElementById('authFail').innerHTML = "";
            if (socketio) 
                socketio.emit('login', username, password);
            document.getElementById('username').value = "";
            document.getElementById('password').value = "";
        }
This allows for users to login - gets the info from what is entered in the chatbox and then sends that information to the server for validation.

Private Chat Server Side
var chatmessage = socketclient.username + " says: " + message + 
        " - message sent at " + time ; //time = timestamp
Allows user to send message privately from server side.

Private Chat Client Side
  function sendPrivateMessage(){
            socketio.emit("privateChat", document.getElementById('recipient').value, document.getElementById('recipientMessage').value);
            document.getElementById('recipientMessage').value = "";
        }
Allows user to send message privately from client side.

Public Chat Server Side
socketio.to(userId).emit('privateChat', chatmessage);
        socketio.to(socketclient.id).emit('privateChat', chatmessage);
    })
Allows user to send message to everyone from server side.

Public Chat Client Side
function sendmessage(){
            socketio.emit("chat", document.getElementById('yourmessage').value);
            document.getElementById('yourmessage').value = "";
        }
Allows user to send message to everyone from client side.

## Deployment

Our messaging app can be deployed using Heroku when we start the system. The link is as follows:
http://team1-messenger.herokuapp.com/
You must login correctly to use our app. The logins are set up with our first name as the username and the password is the last name. 
For example, for professor to login, the username would be "Phu" and the password is "Phung". Our page will not allow you to send messages unless you are logged in.

# Software Process Management

_(Start from Sprint 0, keep updating)_

Introduce how your team uses a software management process, e.g., Scrum, how your teamwork, collaborate

* Our team utilizes Scrum meetings and Agile methodologies to determine requirements, distribute work, and implement solutions. This is applicable for both Sprint 1 and 0.

Include the Trello board with product backlog and sprint cycles in an overview figure and also in detail description. _(Main focus of Sprint 0)_

![Trello Board](https://trello-attachments.s3.amazonaws.com/5f583a567084fd7ed609a641/5f5bc146c3542236c189a495/d724a4fdc14c458893b667f862495634/Team1BoardForREADME.png)























Also, include the Gantt chart reflects the timeline from the Trello board. _(Main focus of Sprint 0)_
![Gantt Chart](https://trello-attachments.s3.amazonaws.com/5f583a567084fd7ed609a641/5f5bc2503a4d6a1d79e449dc/2c0b6e6763d951ea9a0589cbe6a9e814/GanttChart.png)


















## Scrum process

### Sprint 0

Duration: 08/27/2020 - 09/10/2020

#### Completed Tasks: 

1. Established Bitbucket repository
2. Set up Trello Board
3. Decided daily meeting schedule
4. Presented Sprint 0
5. ...
### Sprint 1

Duration: 09/11/2020 - 10/05/2020

###Completed Tasks:
1. Public and Private Chat UI
2. Login and validation of users
3. Timestamp, close window, character limit
4. Presentation

#### Contributions: 

1.  Sydney Jenkins, 10 hours, contributed in use cases, presentation, Bitbucket, Report
2.  David Puzder, 10 hours, contributed in use cases, technology, implementation, Bitbucket
3.  Devin Porter, 10 hours, contributed in use cases, presentation, implementation, Trello
4.  Brandon Wong, 10 hours, contributed in use cases, Bitbucket, implementation, Trello

#### Sprint Retrospective:

_(Introduction to Sprint Retrospective:

_Working through the sprints is a continuous improvement process. Discussing the sprint has just completed can improve the next sprints walk through a much efficient one. Sprint retrospection is done once a sprint is finished and the team is ready to start another sprint planning meeting. This discussion can take up to 1 hour depending on the ideal team size of 6 members. 
Discussing good things happened during the sprint can improve the team's morale, good team-collaboration, appreciating someone who did a fantastic job to solve a blocker issue, work well-organized, helping someone in need. This is to improve the team's confidence and keep them motivated.
As a team, we can discuss what has gone wrong during the sprint and come-up with improvement points for the next sprints. Few points can be like, need to manage time well, need to prioritize the tasks properly and finish a task in time, incorrect design lead to multiple reviews and that wasted time during the sprint, team meetings were too long which consumed most of the effective work hours. We can mention every problem is in the sprint which is hindering the progress.
Finally, this meeting should improve your next sprint drastically and understand the team dynamics well. Mention the bullet points and discuss how to solve it.)_

* Sprint retrospective for Sprint 0:
    * This sprint was generally low stress and largely quite simple in terms of setup and design. The presentation went well, if a little long, and I think we did a good  
      job covering all our bases. Here's a quick overview of what went well and what we can improve on:


|            Good                   |         Could have been better         |              How to improve?                |
|:---------------------------------:|:--------------------------------------:|:-------------------------------------------:|
|* Presentation                     |* Timing of our presentation            |* Practice the presentation before doing it. |  
|* BitBucket Design                 |* More use cases	                      |* Have brainstorming session, ask non-devs   |
|* Everyone doing their part        |* Better communication about commits    |* Meetings to get bulk of same-file work done|
|* Categorizing sprint focuses      |                                        |                                             |

### From Sprint 1
This sprint was much more involved than Sprint 0 was obviously.  We had to do much more implementation and actual coding which was more difficult. We felt that our presentaion went really well overall although we were rushed considering we were the last group to present. We want to improve on our user interface to make our webpage aesthetically more pleasing.
What we did well on:
Implementation of private and public chat
Presentation
Bitbucket design / trello

Improve:
CSS/User interface
Time management - we waited a little late this sprint to get things moving

We plan to meet and do more work together on zoom earlier before the Sprint is due so that we are not as rushed as we were this time. 


### Evaluation

Screenshots of webpage 
### Webpage logged in
![Logged In Screenshot](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511ae002541e29b70dcdff/7ee6897bb08d2c9872a1bc3c2b45aae9/Logged_In_Screenshot.png)
### Private Chat Homepage
![Private Chat Screenshot](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511a6895e1fb713de81f0f/82b17e53e35e8cce0b8a67b192bb9233/Private_Chat_Screenshot.png)


* Other community or team contact