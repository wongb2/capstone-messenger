var http = require('http')
var express = require('express')
var app = express()

app.use(express.static('public'))
var server = http.createServer(app)
const port = process.env.PORT || 8080
server.listen(port);
console.log(`Express HTTP Server is listening at port ${port}`)

app.get('/', (request, response) => {
  console.log("Got an HTTP request")  
  response.sendFile(__dirname+'/index.html')
})

var io = require('socket.io');
var socketio = io.listen(server);
var userList = [];
console.log("Socket.IO is listening at port: " + port);
socketio.on('connection', function(socketclient){
    console.log("A New Socket.io client is connected. ID=" + socketclient.id);

    socketclient.on('login', async (username,password) => {
        if (username.length > 30){
            console.log("Server username too big");
            return;
        }
        if (username.length < 1){
            console.log("Server username too small");
            return;
        }
        if (password.length > 20){
            console.log("Server username too big");
            return;
        }
        console.log("Debug>Got username= "+username + "; password= "+password);
        var checklogin = await DataLayer.checklogin(username,password)
        if(checklogin){
            socketclient.authenticated=true;
            socketclient.emit("authenticated");
            DataLayer.addUserList(username, socketclient.id);

            socketclient.username = username;
            var welcomemessage = username + " has joined the chat system!";
            console.log(welcomemessage);

            console.log(userList);
            var groupArray = await messengerdb.loadGroupChats(username);
            console.log(groupArray.length);
            for (var i = 0;i<groupArray.length;i++){
                socketio.to(socketclient.id).emit("acceptInvite", groupArray[i]);
            }

            SendToAuthenticatedClient(socketclient,"welcome", welcomemessage);
            SendToAuthenticatedClient(undefined,"displayUsers", userList);

            //send the array of group chats to the frontend so it can loop through them and be added to all the groups.
            //SendToAuthenticatedClient(undefined, "groupArray", data);

            // Retrieves public chat history from database, reverses it and emits it to index.html
            var chat_history = await messengerdb.loadChatHistory(username)
            if (chat_history && chat_history.length > 0) {
                chat_history = chat_history.reverse()
                //reverse the order as we get the latest first
                socketclient.emit("chat_history",chat_history)
            }

            // Retrieves private chat history from database, reverses it and emits it to index.html
            var chat_history = await messengerdb.loadPrivateChatHistory(username)
            if (chat_history && chat_history.length > 0) {
                chat_history = chat_history.reverse()
                //reverse the order as we get the latest first
                socketclient.emit("private_chat_history",chat_history)
            }
        }
        else{
            socketclient.authenticated=false;
            socketclient.username=username;
            var welcomemessage = username + " failed to authenticate."
            console.log(welcomemessage);
            socketclient.emit('authFail');
        }
        
    })
    
    socketclient.on('logout', (username) => {
            console.log("Debug>logout");
            socketclient.authenticated=false;
            socketclient.username=username;
            var logoutmessage = username + " has logged out. Goodbye!"
            SendToAuthenticatedClient(socketclient,"welcome", logoutmessage);
            console.log(logoutmessage);
                    DataLayer.removeUser(socketclient.id);
                    SendToAuthenticatedClient(undefined,"displayUsers", userList);
                    socketclient.emit('loggedOut');
    });

    socketclient.on('disconnect', function() {
        DataLayer.removeUser(socketclient.id);
        console.log(socketclient.username+" has left the page.")
        console.log(userList);
        SendToAuthenticatedClient(undefined,"displayUsers", userList);
    });
    socketclient.on('deleteAccount', async () => {
        var checkDelete = await DataLayer.removeAccount(socketclient.id);
        if(checkDelete){
            socketclient.emit("accountDeleted");
        }
    });

    socketclient.on("register",(username,password)=> {
        if (username.length < 1){
            console.log("Server username too small");
            return;
        }
        if (username.length > 30){
            console.log("Server username too big");
            return;
        }
        if (password.length > 20){
            console.log("Server password too big");
            return;
        }
        //Call the Data Layer to register
        DataLayer.addUserAccount(username,password,(result)=>{
            socketclient.emit("registration",result)
        })
    });

    socketclient.on("changePass", (oldpassword,newpassword)=> {
        if (oldpassword.length > 20){
            console.log("Server password too big");
            return;
        }
        if (oldpassword.length == 0){
            console.log("Server password too small");
            return;
        }
        if (newpassword.length > 20){
            console.log("Server password too big");
            return;
        }
        if (newpassword.length < 8){
            console.log("Server password too small");
            return;
        }
        console.log("Debug>Got oldpassword= "+oldpassword + "; newpassword= "+newpassword);
        DataLayer.checkPasswords(socketclient.id,oldpassword,newpassword, (result)=>{
            socketclient.emit("passwordChanged", result);
        });
    });

    socketclient.on("chat", (message, fromNotify=false, erNotify=false) => {
        if(!socketclient.authenticated){
            console.log("Unauthenticated Client tried to send a message.")
            return false;
        }
        if(message.length > 250){
            console.log("Message too big")
            return false;
        }

        var chatmessage = socketclient.username + " says: " + message;

        if(fromNotify){
            console.log("This is a typing notification.")
            chatmessage=socketclient.username+message;
            console.log(chatmessage);
            SendToAuthenticatedClient(undefined,"pubType", chatmessage);
        }
        else if(erNotify){
            SendToAuthenticatedClient(undefined, "erasePubTyping", "")
        }
        else {
            console.log(chatmessage);
            SendToAuthenticatedClient(undefined,"chat", chatmessage);
        }
        
    })
    /* no longer used 
    send reaction to all connected clients
    socketclient.on("react", (reaction) =>{
        console.log(reaction);
        console.log("Debug on react") ; 
        SendToAuthenticatedClient(undefined, "react", reaction);
    })
    */
    
    socketclient.on("privateChat", (username, message, fromNotify=false, erNotify=false) => {
        if(!socketclient.authenticated){
            console.log("Unauthenticated Client tried to send a message.")
            return false;
        }
        if(message.length > 250){
            console.log("Server Message too big")
            return false;
        }
        if (username.length > 30){
            console.log("Server username too big");
            return false;
        }
        // Format of the actual chat message itself
        var chatmessage = socketclient.username + " says to " + username + ": " + message;
        console.log(chatmessage);

        index = -10;
        for (i = 0; i < userList.length; i++) {
            if(userList[i].key === username){
                index = i;
                break;
            }
        }
        if (index === -10){
            DataLayer.checkUser(username, (result)=>{
                // When user exists, but is offline
                if(result === "UserExist"){     
                    console.log("Error, no user by that username logged in, but they do exist.");
                    socketio.to(socketclient.id).emit('privateChat', chatmessage, true);
                    messengerdb.storePrivateChat(socketclient.username, username, chatmessage, false);
                    return false;
                } 
                // When user does not exist
                else {    
                    console.log("Error, no user by that username exists");
                    socketio.to(socketclient.id).emit('privateChatNotExist', username);
                    return false;
                }
            });
        } else {
            console.log("index" + index);
            userId=userList[index].value;
            console.log(userId);
        // if(fromNotify){
        //     console.log("This is a typing notification.")
        //     socketio.to(userID).emit('privType', socketclient.username+" is typing...");
        //     socketio.to(socketclient.id).emit('privType',socketclient.username + " is typing...")
        // }
        // else if(erNotify){
        //     socketio.to(userID).emit('privErase', "");
        //     socketio.to(socketclient.id).emit('privErase',"");
        // }

            // When user exists and is online
            socketio.to(userId).emit('privateChat', chatmessage);
            socketio.to(socketclient.id).emit('privateChat', chatmessage, true);
            messengerdb.storePrivateChat(socketclient.username, username, chatmessage, true);
        }
    });
 
    // Read receipts for private chat
    socketclient.on("read_receipt", async (chat_history, lastIndex) => {
            console.log("Debug>Updating read receipts for old messages");
            var receiptOutput = await messengerdb.updateReceipt(chat_history, socketclient.username)

            console.log(lastIndex);
    });

    socketclient.on("getUsers", function(){
        console.log("Debug>Received getUsers call.");
        console.log(userList);
        // for (i = 0; i < userList.length; i++) {
        //     console.log(userList);
        // }
        SendToAuthenticatedClient(undefined,"displayUsers", userList);
    });

    socketclient.on("joinRoom",(room)=>{
        socketclient.join(room);
        messengerdb.storeUserGroups(socketclient.username, room);
        console.log(socketclient.username + " has been added to "+room);
    });
    socketclient.on("addToRoom", (username, room) => {
        //find the username in the userList, send the socket with that user an "acceptInvite" event.
        console.log("Inviting "+username+" to the room "+room);
        index=-10;
        for (i = 0; i < userList.length; i++) {
            if(userList[i].key === username){
                index = i;
                break;
            }
        }
        if (index === -10){
            console.log("Error, no user by that username logged in");
        }
        console.log("index" + index);
        userId=userList[index].value;
        console.log(userId);
        
        //send the acceptInvite. When the person who we want to add to a room receives it, they will call joinRoom, and enter the room.
        socketio.to(userId).emit("acceptInvite", room);
    });

    socketclient.on("roomMessage", (room, message)=>{
        console.log(room)
        message = socketclient.username + ": " + message;
        console.log("Sending '"+message+"' from "+socketclient.username +" in room "+room);
        socketio.to(room).emit("roomMessage", room, message);
        messengerdb.storeGroupChat(socketclient.username, message, room);
    });

    socketclient.on("getGroupChatHistory", async (room) =>{
        var chat_history = await messengerdb.loadGroupChatHistory(room)
        if (chat_history && chat_history.length > 0) {
            chat_history = chat_history.reverse()
            //reverse the order as we get the latest first
            var roomMessageDiv = room+"Messages";
            socketclient.emit("group_chat_history",roomMessageDiv,chat_history)
        }
    });

});
var messengerdb=require("./messengerdb")
var DataLayer = {
    info: 'Data Layer Implementation for Messenger',
    async checklogin(username, password){
        var checklogin_result = await messengerdb.checklogin(username,password)
        console.log("Debug>DataLayer.checklogin->result="+ checklogin_result)
        return checklogin_result
    },
    checkUser(username,callback){
        messengerdb.checkUser(username,(result) =>{
            callback(result)})
    },
    async removeAccount(socketId){
        for (i = 0; i < userList.length; i++) {
            if(userList[i].value === socketId){
                index = i;
                break;
            }
        }
        console.log("Debug>DataLayer.removeAccount-> username="+userList[index].key)
        var checkdelete_result = await messengerdb.removeUser(userList[index].key)
        console.log("Debug>DataLayer.removeAccount-> result="+checkdelete_result)
        this.removeUser(socketId);
        return checkdelete_result;
    },
    checkPasswords(socketId, oldpassword, newpassword, callback){
        for (i = 0; i < userList.length; i++) {
            if(userList[i].value === socketId){
                index = i;
                break;
            }
        }
        console.log("Debug>DataLayer.checkPasswords-> username="+userList[index].key)
        messengerdb.passwordChange(userList[index].key, oldpassword, newpassword, (result) =>{
            callback(result)
        })
    },
    addUserAccount(username,password,callback){
        messengerdb.addUser(username,password,(result) =>{
            callback(result)})
    },
    addUserList(username, socketId){
        userList.push({
            key:   username,
            value: socketId
        });
        console.log("Number of users " + userList.length);
    },
    removeUser(socketId){
        index=-10;
        if (userList.length === 1){
            console.log("Emptied the userlist, there was only one element.");
            userList.pop();
        } else {
            for (i = 0; i < userList.length; i++) {
                if(userList[i].value === socketId){
                    index = i;
                    break;
                }
            }
            userList.splice(index, 1);
        }
        console.log("Number of users " + userList.length);
    }
}


function SendToAuthenticatedClient(sendersocket,type,data){
    var sockets = socketio.sockets.sockets;
    for(var socketId in sockets){
        var socketclient = sockets[socketId];
        if(socketclient.authenticated){
            socketclient.emit(type,data);
            var logmsg= "Debug:>sent to " + socketclient.username + " with ID=" + socketId;
            console.log(logmsg);
            if(type==="chat") //New use case: store the chat message
                messengerdb.storePublicChat(socketclient.username, data);
        }
    }
}