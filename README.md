University of Dayton

Department of Computer Science

CPS 490 - Capstone I, Fall 2020

Instructor: Dr. Phu Phung


## Capstone I Project 


# The Messenger Application


# Team members

* Brandon Wong <wongb2@udayton.edu>
* David Puzder <puzderd1@udayton.edu>
* Sydney Jenkins <jenkinss4@udayton.edu>
* Devin Porter <porterd3@udayton.edu>


# Project Management Information

Management board (private access): <https://trello.com/b/aqMlNbvH/team-1-sprint-board>

Source code repository (private access): <https://bitbucket.org/cps490f20-team1/cps490-project-team1/src/master/>


## Revision History

| Date     |   Version     |  Description |
|----------|:-------------:|-------------:|
|09/10/2020|  0.0          | Sprint 0     |
|10/02/2020| 0.1		   | Sprint 1	  |


# Overview

![ThreeTier](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f4e9328cca2c93601be8c9d/71588b287bcb31a71ac43924cd7c89d7/ThreeTier.JPG)

Technology?
– Node.js

Type of software?
– Web Application with WebSocket
– Use a web browser
– Accessed through a URL
– Reside on a Web server
– Use standard IP protocols

How many layers?
– Client/Server/Databse 
– Traditional three-tiered architecture
– Software design with part of the application on a server, 
part on the client, part with the database

# System Analysis

Sprint 1
Our system is made up of the backend web server (ChatServer.JS) and the front end client interface (Index.html. We have multiple ChatUI pages that are used for public and private chat.
The saved usernames and passwords are saved in a Dictionary in the ChatServer.js file that allows for users to be validated as registered.
We use Socket.io to allow real-time bidirectional communication between the browser and the server, using Websocket.

## User Requirements

* Send Direct Message
	* Users can type, text, and send to a single receiver
	* Description: User inputs name of another user in the system. The system sends message input and logs both usernames, time sent, time recieved, 
	* and message content. 
* Public Chat
	* Users can type, text, and send to a public chatroom with a character limit
	* Description: User enters the public chatroom and user sends message and all members of chatroom recieve it.
* Registration
	* Users can register to the system
	* Description: Users enter new account data and the system assigns a new account number, a new user record, and new account record.
* Login
	* Users can login with registered account and close window when finished
	* Description: User enters already registered account data and the system retrieves account and record information.
* Real-Time Messaging
	* Users should receive the messages sent to them in real time with a timestamp
	* Description: Users enter another user's name and can input message to send to another user. Can also retrieve messages sent from other users.
	* System creates record of times sent and recieved and allows messages back and forth almost instantly.
* Message History
	* Users can view the message history
	* Description: Users can select message history to view records of previously sent messages between users in the system.
* Read Receipts
	* Users can see if the sent messages have been read
	* Description: Users can select messages to see if messages were viewed by whom they were sent to and if viewed, they can see the time the message was viewed.

## Use cases

User Stories:

* Register for an account
	* As an unregistered user, I am able to register for a new account within the system.
* Login
	* As a registered user, I can log into the system using a username and password.
* Send Direct Message
	* As a registered user, I can send a message directly to another registered user, securely and without any other user viewing that message. 
* View message history
	* As a registered user, I can review previous messages that I have sent, and have been sent to me, whether in group or direct messaging.
* Send Public Message
	* As a registered user, I can send a message to all logged in users at once, and any messages sent back to me will also go to all of those people.
* Real-Time Messaging
	* As a registered user, I can receive messages as they are sent to me.
* Read Receipts
	* As a user who has sent a message, I can see who has and has not read the message.






Overview Use Case Diagram
![Use Case Diagram](https://trello-attachments.s3.amazonaws.com/5f514012901b49388c7d232d/894x992/3f8e26a350204116060680940ee1dcb8/Screen_Shot_2020-09-25_at_5.04.13_PM.png)
	
Use Case Discriptions
![Registration Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b01268db90b1eb8e22e/fcacedf9f71a9a9e8d39f9a094859312/Screen_Shot_2020-09-25_at_5.12.11_PM.png)
![Login Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b209807b273a0e3c158/71444c6d535d3e34da77ca57a9217e79/Screen_Shot_2020-09-25_at_5.12.48_PM.png)
![Receive a message Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b3fb02560171c51d795/c7388a65cdbecf937305a6a7863f45dc/Screen_Shot_2020-09-25_at_5.15.39_PM.png)
![Send a Direct Message Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511a6895e1fb713de81f0f/22dab9edd1cfb99dc2874707d64bb0d4/Screen_Shot_2020-09-25_at_5.09.47_PM.png)
![Send to the Receiver Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511a6895e1fb713de81f0f/9faac0085415978c0d80e0a01c3e89f8/Screen_Shot_2020-09-25_at_5.10.00_PM.png)
![View Message History Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b83e88aef2e54f93a8e/b88330bc7e0f7d8391929b2786bac735/Screen_Shot_2020-09-25_at_5.13.42_PM.png)
![View Read Receipts Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511b9f550c903090e633fb/7b8cfd0a585fb13299787c7895b6652b/Screen_Shot_2020-09-25_at_5.14.14_PM.png)
![Send Public Message Use Case Discription](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511ae002541e29b70dcdff/aabaf909d2bc57c45f1e995ed703be79/Screen_Shot_2020-09-25_at_5.11.33_PM.png)


# System Design

SPRINT 1
Currently, our system is designed to be a messenger app for registered users to chat publicly and privately as they please. Users log in with a username and password and once they are verified, the chat box appears and they can send and recieve messages. If they would like to chat privately, they select the button that says that and then enter the name of the recipient whom they wish to message. When a message is sent, there is a timestamp. When a user joins the chat, their name appears up top. When they are done chatting, they can select to "close window" which closes out the tab. We will continue to update as we progress through the sprints.

### Login Sequence Diagram
![Login Sequence Diagram](https://trello-attachments.s3.amazonaws.com/5f58131b90edaf09eab4ac6b/5f6e60b732e8a517fe529fc9/5e0783d32d77c7046c05b897cd6fd77b/Login_Sequence_Diagram.png)

### Public Chat Sequence Diagram
![Public Chat Sequence Diagram](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f511ae002541e29b70dcdff/3e8e57f0480e39da6a1f98d2967fcf1c/Public_Chat_Sequence_Diagram.png)

SPRINT 2 

### Register Sequence Diagram
![Register Sequence Diagram](https://trello-attachments.s3.amazonaws.com/5f58131b90edaf09eab4ac6b/5f96eb6d2cce084c65e12d2e/c1480c0b700afe366126f2c08d5c4edf/register.JPG)


### Group Chat Sequence Diagram
![Group Chat Sequence Diagram](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f7cbb47522a5d3ecf18ccca/8cde7a841f8e4ac6ca82c50898b81206/Group_Chat_Sequence_Diagram.png)

## Use-Case Realization

We have implemented all the processes identified in the Use Cases in our webpage. This is evident in the implementation and the page itself.

## Database 


## User Interface

SPRINT 1 
Right now, our webpage is pink with a username and password block for users to login. Once they are validated as registered users, a seperate chat box appears that allows them to send messages. This chatbox is hidden from them until they login correctly. This is to keep the page clean. When a user logs in, their username appears in a welcome message. The user list is seperate from the chat box to keep the page clean and easy to read. Users can select to go to private chat from a seperate button. When they do this, it takes them to a different ChatUI, as indicated in the instructions. 

SPRINT 2
Our interface now has a register user function and a logout. When users are logged in, they have the option to message a groupchat in a session chat window. There is a userlist that tells them who is in the chat and who they can mesage. When users logout, they recieve an alert that tells them they must login again and then they cannot access the messenger until they login again.  Also, our page is not pink anymore which is sad.

# Implementation


### SPRINT 1
For this sprint, we are implementing the use of NodeJs to create our chat messenging system. The ChatServer.Js file is the server program that allows for dynamic content. This is what allows us to message across various pages. The Index.html is the client interface that displays the webpage to the user. We use JavaScript in this file to run in the web browser.

Login Server Side:
var DataLayer = {
    info: 'Data Layer Implementation for Messenger',
    checklogin(username, password){
        var userDict={"Devin":"Porter", "David":"Puzder", "Sydney":"Jenkins", "Brandon":"Wong", "Phu":"Phung", "Anna":"Duricy"};
        console.log("checklogin: "+username+ "/" + password);
        if (password === userDict[username]){
            return true;
        }
		
This is what allows users to login from the server side (ChatServer.js). The username and password combination are saved in the dictionary. When the password matches the username, the DataLayer returns true and the user is validated. 

Login Client Side:
   function login(){
            var username = document.getElementById('username').value;
            var password = document.getElementById('password').value;
            document.getElementById('authFail').innerHTML = "";
            if (socketio) 
                socketio.emit('login', username, password);
            document.getElementById('username').value = "";
            document.getElementById('password').value = "";
        }
This allows for users to login - gets the info from what is entered in the chatbox and then sends that information to the server for validation.

Private Chat Server Side
var chatmessage = socketclient.username + " says: " + message + 
        " - message sent at " + time ; //time = timestamp
Allows user to send message privately from server side.

Private Chat Client Side
  function sendPrivateMessage(){
            socketio.emit("privateChat", document.getElementById('recipient').value, document.getElementById('recipientMessage').value);
            document.getElementById('recipientMessage').value = "";
        }
Allows user to send message privately from client side.

Public Chat Server Side
socketio.to(userId).emit('privateChat', chatmessage);
        socketio.to(socketclient.id).emit('privateChat', chatmessage);
    })
Allows user to send message to everyone from server side.

Public Chat Client Side
function sendmessage(){
            socketio.emit("chat", document.getElementById('yourmessage').value);
            document.getElementById('yourmessage').value = "";
        }
Allows user to send message to everyone from client side.

### SPRINT 2
Server Side Logout
  socketclient.on('logout', (username) => {
            console.log("Debug>logout");
            socketclient.authenticated=false;
            socketclient.username=username;
            var logoutmessage = username + " has logged out. Goodbye!"
            SendToAuthenticatedClient(socketclient,"welcome", logoutmessage);
            console.log(logoutmessage);
                    DataLayer.removeUser(socketclient.id);
                    socketclient.emit('loggedOut');
    });
Sends to server that user has logged out.

Client Side Logout
function logout(){
            //alert("you have clicked logout"); 
            var username = document.getElementById('username').value;
            socketio.emit('logout', username);
            document.getElementById('loggedOut').innerHTML = "";
        }
		
Alert when the user has logged out.

Server Side Register
  socketclient.on("register",(username,password)=> {
        //Call the Data Layer to register
        DataLayer.addUserAccount(username,password,(result)=>{
            socketclient.emit("registration",result)
        })
    });
Adds new user to database

Client Side Register
      function register(){
            var username = document.getElementById('newusername').value;
            var password = document.getElementById('newpassword').value;
            //TODO: validate username/password
            socketio.emit("register",username,password);
        }
Called when new user attempts to register

## Deployment

Our messaging app can be deployed using Heroku when we start the system. The link is as follows:
http://team1-messenger.herokuapp.com/
You must login correctly to use our app. The logins are set up with our first name as the username and the password is the last name. 
For example, for professor to login, the username would be "Phu" and the password is "Phung". Our page will not allow you to send messages unless you are logged in.

# Software Process Management

Introduce how your team uses a software management process, e.g., Scrum, how your teamwork, collaborate

* Our team utilizes Scrum meetings and Agile methodologies to determine requirements, distribute work, and implement solutions. This is applicable for both Sprint 1 and 0.

Include the Trello board with product backlog and sprint cycles in an overview figure and also in detail description. _(Main focus of Sprint 0)_

![Trello Board](https://trello-attachments.s3.amazonaws.com/5f583a567084fd7ed609a641/5f5bc146c3542236c189a495/d724a4fdc14c458893b667f862495634/Team1BoardForREADME.png)























Also, include the Gantt chart reflects the timeline from the Trello board. _(Main focus of Sprint 0)_
![Gantt Chart](https://trello-attachments.s3.amazonaws.com/5f583a567084fd7ed609a641/5f5bc2503a4d6a1d79e449dc/2c0b6e6763d951ea9a0589cbe6a9e814/GanttChart.png)


















## Scrum process

### Sprint 0

Duration: 08/27/2020 - 09/10/2020

#### Completed Tasks: 

1. Established Bitbucket repository
2. Set up Trello Board
3. Decided daily meeting schedule
4. Presented Sprint 0
5. ...
### Sprint 1

Duration: 09/11/2020 - 10/05/2020

### Completed Tasks:
1. Public and Private Chat UI
2. Login and validation of users
3. Timestamp, close window, character limit
4. Presentation

#### Contributions: 

1.  Sydney Jenkins, 10 hours, contributed in use cases, presentation, Bitbucket, Report
2.  David Puzder, 10 hours, contributed in use cases, technology, implementation, Bitbucket
3.  Devin Porter, 10 hours, contributed in use cases, presentation, implementation, Trello
4.  Brandon Wong, 10 hours, contributed in use cases, Bitbucket, implementation, Trello

#### Sprint Retrospective:


* Sprint retrospective for Sprint 0:
    * This sprint was generally low stress and largely quite simple in terms of setup and design. The presentation went well, if a little long, and I think we did a good  
      job covering all our bases. Here's a quick overview of what went well and what we can improve on:


|            Good                   |         Could have been better         |              How to improve?                |
|:---------------------------------:|:--------------------------------------:|:-------------------------------------------:|
|* Presentation                     |* Timing of our presentation            |* Practice the presentation before doing it. |  
|* BitBucket Design                 |* More use cases	                      |* Have brainstorming session, ask non-devs   |
|* Everyone doing their part        |* Better communication about commits    |* Meetings to get bulk of same-file work done|
|* Categorizing sprint focuses      |                                        |                                             |

### From Sprint 1
This sprint was much more involved than Sprint 0 was obviously.  We had to do much more implementation and actual coding which was more difficult. We felt that our presentaion went really well overall although we were rushed considering we were the last group to present. We want to improve on our user interface to make our webpage aesthetically more pleasing.
What we did well on:
Implementation of private and public chat
Presentation
Bitbucket design / trello

Improve:
CSS/User interface
Time management - we waited a little late this sprint to get things moving

We plan to meet and do more work together on zoom earlier before the Sprint is due so that we are not as rushed as we were this time. 

### Sprint 2
We are so proud of our work in this sprint!! We were much better with time management than we were in Sprint 1. Anna also was a huge help this round. We still need to work on our CSS. Right now that is our biggest issue.
# What we did well on:
*	Implementation of logout and register
*	Presentation
*	Teamwork

# Improve
* CSS

This Sprint we did a lot more together instead of dividing up the requirements and this was a huge help.


### Evaluation

Screenshots of webpage 
### Webpage logged in
![New Home Page](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f4e9328cca2c93601be8c9d/a61ce9107e24faaeb8058936616655cc/New_Home_Page.png)

### Register for an Account
![NewRegister](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f4e9328cca2c93601be8c9d/de0574d8e18ed05bed4eb521657712ac/NewRegister.png)

### Logout Alert
![Logout Alert](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f4e9328cca2c93601be8c9d/b0a5f7c9b903dd1929e2030367a8d649/Logout_Alert.png)

### Change Password
![Change Password](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f4e9328cca2c93601be8c9d/0872a8a175e0560619b11b6343dd74b8/Change_Password.png)

### GROUPCHAT
![Group Chat 1](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f7cbb47522a5d3ecf18ccca/10d90f5fffcb966b1352f998089c63da/Group_Chat_1.png)
![Group Chat 2](https://trello-attachments.s3.amazonaws.com/5f46e661f89cc260dd4c95eb/5f7cbb47522a5d3ecf18ccca/f1fc720c508c1a6c7b174ee2cf310d04/Group_Chat_2.png)

* Other community or team contact