const MongoClient = require('mongodb').MongoClient;
const bcrypt = require("bcryptjs");
const uri = "mongodb+srv://puzderd1:5qt6sIEKP8TqbaVH@messengerdb.2qlrp.mongodb.net/messenger?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}
const checklogin = async (username,password)=>{
    if (username.length > 30){
        console.log("DB username too big");
        return false;
    }
    if (username.length < 1){
        console.log("DB username too small");
        return;
    }
    if (password.length > 20){
        console.log("DB password too big");
        return false;
    }
    //your implementation
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username){
        console.log("Debug>messengerdb.checklogin-> user found:\n" + JSON.stringify(user))
        return bcrypt.compareSync(password, user.password);
    }
    return false
}
const passwordChange = (username,oldpassword,newpassword,callback)=>{
    if (username.length > 30){
        console.log("DB username too big");
        callback("Error");
    }
    if (username.length < 1){
        console.log("DB username too small");
        callback("Error");
    }
    if (oldpassword.length > 20){
        console.log("DB username too big");
        callback("Error");
    }
    //your implementation
    var oldhashedpassword = bcrypt.hashSync(oldpassword,10);
    var newhashedpassword = bcrypt.hashSync(newpassword,10);
    var users = getDb().collection("users");
    users.findOne({username:username,password:oldhashedpassword}).then(user=>{
        if(user && user.username==username && bcrypt.compareSync(oldpassword, user.password)){
            console.log("Debug>messengerdb.passwordChange-> user found:\n" + JSON.stringify(user))
            users.updateOne({username:username,password:oldhashedpassword},{ $set: {password:newhashedpassword } },(err,result)=>{
                 if(err){
                     console.log("Debug>messengerdb.addUser: error for adding " + newhashedpassword +"':\n", err);
                     callback("Error");
                 } else {
                     console.log("Debug>messengerdb.passwordChange: a new password is updated: \n", newhashedpassword);
                     callback("Success")
                 }
            })
        }
    })
    
}
const removeUser = async (username)=>{
    if (username.length > 30){
        console.log("DB username too big");
        return false;
    }
    if (username.length < 1){
        console.log("DB username too small");
        return;
    }
    //your implementation
    var users = getDb().collection("users");
    var user = await users.deleteOne({username:username});
    console.log("Debug>messengerdb.removeUser user=" + JSON.stringify(user));
    if(user.deletedCount==1){
        console.log("Debug>messengerdb.removeUser=True:\n")
        return true
    }
    return false
}
const addUser = (username,password,callback) => {

    if (username.legnth > 30){
        console.log("DB username too big");
        callback("InvalidInput");
    }
    if (username.legnth == 0){
        console.log("Server username too small");
        callback("InvalidInput");
    }
    if (password.legnth > 20){
        console.log("DB password too big");
        callback("InvalidInput")
    }
    var hashedpassword = bcrypt.hashSync(password,10);
     var users = getDb().collection("users");
     users.findOne({username:username}).then(user=>{
         if(user && user.username === username) {
             console.log(`Debug>messengerdb.addUser: Username "${username}" exists!`);
             callback("UserExist");
         } else {
             var newUser = {"username": username,"password" : hashedpassword,"groups":[]};
             users.insertOne(newUser,(err,result)=>{
                 if(err){
                     console.log("Debug>messengerdb.addUser: error for adding " + username +"':\n", err);
                     callback("Error");
                 } else {
                     console.log("Debug>messengerdb.addUser: a new user is added: \n", result.ops[0].username);
                     callback("Success")
                 }
             })
         }
     })
}
const checkUser = (username,callback) => {

    if (username.legnth > 30){
        console.log("DB username too big");
        callback("InvalidInput");
    }
    if (username.legnth == 0){
        console.log("Server username too small");
        callback("InvalidInput");
    }
    
    var users = getDb().collection("users");
    users.findOne({username:username}).then(user=>{
        if(user && user.username === username) {
            console.log(`Debug>messengerdb.checkUser: Username "${username}" exists!`);
            callback("UserExist");
        } else {
            console.log(`Debug>messengerdb.checkUser: Username "${username}" does not exists!`);
            callback("UserNotExist");
        }
    })
}


const storePublicChat = (message)=>{
    //TODO: validate the data and print out debug info
    let timestamp = Date.now();
    let chat = {message:message, timestamp:timestamp};
    getDb().collection("public_chat").insertOne(chat,function(err,doc){
        if(err != null){
            console.log(err);
        }else{
            console.log("Debug: message is added:" + JSON.stringify(doc.ops));
        }
    })
}

const storePrivateChat = (sender,receiver,message,read)=>{
    //TODO: validate the data and print out debug info
    let timestamp = Date.now();
    let chat = {sender: sender, receiver: receiver, message:message, timestamp:timestamp, read:read};
    getDb().collection("private_chat").insertOne(chat,function(err,doc){
        if(err != null){
            console.log(err);
        }else{
            console.log("Debug: private message is added:" + JSON.stringify(doc.ops));
        }
    })
}
const storeGroupChat = (sender, message, room)=>{
    let timestamp = Date.now();
    let chat = {sender: sender, message:message, room:room, timestamp:timestamp};
    getDb().collection("group_chat").insertOne(chat,function(err,doc){
        if(err != null){
            console.log(err);
        }else{
            console.log("Debug: group message is added:" + JSON.stringify(doc.ops));
        }
    })
}

const storeUserGroups = (username, groupName) =>{
    getDb().collection("users").updateOne({username:username}, {$addToSet: {groups: groupName}});
}

// Read Receipts for private chat
const updateReceipt = async (chat_history, receiverName)=>{
    // find a way to incorporate the parameter chat_history and update receipts only when the receiver joins the chat
    getDb().collection("private_chat").updateMany({receiver: receiverName}, {$set: {read: true}});
    return chat_history
}

const loadPrivateChatHistory = async (username, limits=100)=> {
    var chat_history = await getDb().collection("private_chat").find({ $or:[ {sender:username},{receiver:username}]}).sort({timestamp:-1}).limit(limits).toArray();
    //print debug info e.g., using JSON.stringify(chat_history)
    if (chat_history && chat_history.length > 0) return chat_history
}

const loadChatHistory = async (receiver, limits=100)=> {
    var chat_history = await getDb().collection("public_chat").find({}).sort({timestamp:-1}).limit(limits).toArray();
    //print debug info e.g., using JSON.stringify(chat_history)
    if (chat_history && chat_history.length > 0) return chat_history
}

const loadGroupChats = async (username)=>{
    var user = await getDb().collection("users").findOne({username:username});
    console.log(user.username);
    console.log(user.groups);
    return user.groups;
}
const loadGroupChatHistory = async (room, limits=100)=>{
    var chat_history = await getDb().collection("group_chat").find({room:room}).sort({timestamp:-1}).limit(limits).toArray();
    //print debug info e.g., using JSON.stringify(chat_history)
    if (chat_history && chat_history.length > 0) return chat_history
}
module.exports = {checklogin,passwordChange,removeUser,addUser,storePublicChat, loadChatHistory, storePrivateChat, loadPrivateChatHistory, updateReceipt, checkUser, loadGroupChats, storeUserGroups, loadGroupChatHistory, storeGroupChat};